<?php

/*
* Written by Michael Karbowiak
*
* The script requires the following to be installed
* youtube-dl: pip install youtube-dl
* ffmpeg / avconv: apt-get install ffmpeg / yum install ffmpeg etc.
*/

// Define the channels to get data from
$channels = array(
	"MrSuicideSheep" => "https://gdata.youtube.com/feeds/api/users/MrSuicideSheep/playlists?v=2",
);
// Define the format
$format = "mp3";
// Define the quality (0 = best, 9 = worst)
$quality = 0;
// Define the dir to store the files in
$dir = "/storage/Music/youtube/";

foreach($channels as $user => $url)
{
	// Get the xml data
	$data = file_get_contents($url);

	// Define the dir
	$userDir = $dir . $user . "/";

	// Make sure there is actually some data..
	if(!empty($data))
	{
		// Try to run the script, if there is an error, bug out
		try
		{
			// Load the XML
			$xml = simplexml_load_string($data);

			// Foreach entry download!
			foreach($xml->entry as $playlist)
			{
				// Define the title
				$playlistName = $playlist->title;

				// Define the dir
				$chanDir = $userDir . urlencode(str_replace(":", " -", str_replace("#", "", str_replace("&", "", str_replace(" ", "", str_replace("/", "", $playlistName)))))) . "/";

				// Define the URL
				$playlistUrl = $playlist->content["src"];

				// Get the playlist data
				$playlistData = file_get_contents($playlistUrl);

				$playlistXml = simplexml_load_string($playlistData);

				foreach($playlistXml->entry as $song)
				{
					$songTitle = $song->title;
					$songUrl = $song->link["href"];

					// If the file doesn't exist already, download, convert and store it !
					if(!file_exists($chanDir . str_replace(":", " -", $songTitle) . "." . $format))
					{
						echo "Now downloading: {$playlistName} / {$songTitle}\n";
						echo $chanDir."\n";
						exec("youtube-dl -x -q --audio-format {$format} --audio-quality {$quality} -o '{$chanDir}%(title)s.%(ext)s' $songUrl");
					}

				}
			}
		}
		catch(Exception $e)
		{
			// There was an error /o\
			echo "There was an error: " . $e->getMessage();
		}
	}
}
